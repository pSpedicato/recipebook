import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  pageSelected: string = 'recipe';
  onNavigate(selected: string) {
    this.pageSelected = selected;
  }
  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyCSzoKk1HSrpwr13AOblj5hzDuv3pWjV-Q',
      authDomain: 'recipe-book-5ec29.firebaseapp.com',
    });
  }
}
