import { Injectable, EventEmitter } from '@angular/core';
import { Ingredient } from './ingredient.model';
import { Subject } from 'rxjs';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {
  startedEdit = new Subject<number>();
  ingredients: Ingredient[] = [
    new Ingredient('Strawberry', 1),
    new Ingredient('Potato', 10),
    new Ingredient('Apples', 4)
  ];
  ingredientsChanged = new EventEmitter<Ingredient[]>();

  constructor() { }


  addIngredient(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
    this.ingredientsChanged.emit(this.ingredients.slice());

  }
  updateIngredient(ingredient: Ingredient, index: number) {
    this.ingredients[index] = ingredient;

    this.ingredientsChanged.emit(this.ingredients.slice());
  }
  setIngredient(ingredients: Ingredient[]) {
    this.ingredients = ingredients;

    this.ingredientsChanged.emit(this.ingredients.slice());
  }
  getIngredient(index: number) {
    return this.ingredients[index];
  }

  getIngredients() {
    return this.ingredients.slice();
  }

  deleteIngredient(index: number) {
    this.ingredients.splice(index, 1);
    this.ingredientsChanged.emit(this.ingredients.slice());
  }

}
