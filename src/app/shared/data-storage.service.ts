import { Injectable } from '@angular/core';
import { RecipeService } from './recipe.service';
import { ShoppingListService } from './shopping-list.service';
import { AuthService } from '../auth/auth.service';

import { map } from 'rxjs/operators';
import { Recipe } from '../recipes/recipe.model';
import { Ingredient } from './ingredient.model';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import { ResponseType } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  constructor(private recipeService: RecipeService, private slService: ShoppingListService,
    private httpClient: HttpClient) { }


  getRecipes() {
    this.httpClient.get<Recipe[]>('https://recipe-book-5ec29.firebaseio.com/recipe.json', {
      observe: 'body',
      responseType: 'json',
    })
      .pipe(map(
        (recipes) => {
          for (let recipe of recipes) {
            if (!recipe['ingredients']) {
              console.log(recipe);
              recipe['ingredients'] = [];
            }
          }
          return recipes;
        }
      ))
      .subscribe(
        (recipes: Recipe[]) => {
          this.recipeService.setRecipes(recipes);
        }

      );

  }

  saveRecipe() {
    // const headers = new HttpHeaders();
    // return this.httpClient.put('https://recipe-book-5ec29.firebaseio.com/recipe.json', this.recipeService.recipes,
    //   {
    //     observe: 'body',
    //     // headers: headers,
    //     params: params,
    //   });
    const req = new HttpRequest('PUT', 'https://recipe-book-5ec29.firebaseio.com/recipe.json', this.recipeService.recipes,
      {
        reportProgress: true,
      });

    return this.httpClient.request(req);
  }


  saveSList() {
    return this.httpClient.put('https://recipe-book-5ec29.firebaseio.com/slist.json', this.slService.ingredients, {
      observe: 'body',
    });
  }

  fetchSList() {
    return this.httpClient.get<Ingredient[]>('https://recipe-book-5ec29.firebaseio.com/slist.json', {
    })
      .pipe(
        map(
          (ingredients) => {
            return ingredients;
          }
        )
      )
      .subscribe(
        (ingredients: Ingredient[]) => {
          this.slService.setIngredient(ingredients);
        }
      );
  }
}
