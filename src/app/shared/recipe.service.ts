import { Injectable, EventEmitter } from '@angular/core';
import { Recipe } from '../recipes/recipe.model';
import { Ingredient } from './ingredient.model';
import { ShoppingListService } from './shopping-list.service';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  ingredient: Ingredient = new Ingredient('uova', 2);
  recipes: Recipe[] = [
    new Recipe('Carbonara', 'La pasta alla carbonara è un piatto caratteristico del Lazio, e più in particolare di Roma preparato con ingredienti popolari e dal gusto intenso. I tipi di pasta tradizionalmente più usati sono gli spaghetti o i rigatoni.',
      'https://www.giallozafferano.it/images/ricette/175/17567/foto_hd/hd450x300.jpg',
      [new Ingredient('egg', 6), new Ingredient('cheese', 1), new Ingredient('pepe', 1), new Ingredient('pasta', 1), new Ingredient('guanciale', 1)]),
    new Recipe('Amatriciana', "L'amatriciana (matriciana in romanesco) è un condimento per la pasta che ha preso il nome da Amatrice, cittadina in provincia di Rieti, nella regione Lazio.", 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Bucatini_allamatriciana.jpg/520px-Bucatini_allamatriciana.jpg', [new Ingredient('tomato sauce', 1), new Ingredient('pasta', 1), new Ingredient('guanciale', 1)]),
    new Recipe('Cacio e pepe', 'La cacio e pepe è un piatto di pasta della cucina romana. Come suggerisce il nome gli ingredienti del piatto sono il cacio e il pepe', 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Cacio_e_pepe.jpg/500px-Cacio_e_pepe.jpg', [new Ingredient('cacio', 1), new Ingredient('pasta', 1), new Ingredient('pepe', 1)])
  ];
  recipeSelected = new EventEmitter<Recipe>();
  recipeChanged = new EventEmitter<Recipe[]>();

  constructor(private slService: ShoppingListService) { }

  onRecipeSelected(recipeSelected: Recipe) {
    this.recipeSelected.emit(recipeSelected);
  }
  addRecipe(r: Recipe) {
    this.recipes.push(r);
    this.recipeChanged.emit(this.recipes);
  }
  getRecipe(id: number) {
    // const recipe = this.recipes.find(
    //   (r) => {
    //     return r.name === name;
    //   }
    // );
    // return recipe;
    return this.recipes[id];
  }

  update(index: number, recipe: Recipe) {
    this.recipes[index] = recipe;
  }

  addToShoppingList(ingredients: Ingredient[]) {
    let i;
    for (i = 0; i < ingredients.length; i++) {
      this.slService.addIngredient(ingredients[i]);
    }
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
  }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipeChanged.emit(this.recipes);

  }

}
