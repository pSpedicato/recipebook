import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShoppingListComponent } from './shoppingList.component';
import { ShoppingListEditComponent } from './shoppingListEdit/shoppingListEdit.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [
    ShoppingListComponent,
    ShoppingListEditComponent,
  ]
})
export class ShoppingListModule { }
