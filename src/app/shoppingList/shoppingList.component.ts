import { Component, OnInit, OnDestroy } from '@angular/core';
import { templateSourceUrl } from '@angular/compiler';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shared/shopping-list.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';

@Component({
    selector: 'app-shoppingList',
    templateUrl: './shoppingList.component.html'
})

export class ShoppingListComponent implements OnInit {
    ingredients: Ingredient[] = [];
    constructor(private shoppingListService: ShoppingListService, private router: Router, private authService: AuthService) { }

    ngOnInit() {

        this.ingredients = this.shoppingListService.getIngredients();
        this.shoppingListService.ingredientsChanged.subscribe(
            (ingredientList: Ingredient[]) => { this.ingredients = ingredientList; }
        );
    }

    isAuthenticated() {
        return this.authService.isAuthenticated();
    }

    onClickEdit(index: number) {
        // this.router.navigate(['/shopping',index,'edit']);
        this.shoppingListService.startedEdit.next(index);
    }

}
