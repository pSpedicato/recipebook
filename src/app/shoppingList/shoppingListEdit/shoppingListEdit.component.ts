import { Component, ViewChild, ElementRef, EventEmitter, Output, OnInit, OnDestroy, OnChanges } from "@angular/core";
import { Ingredient } from "src/app/shared/ingredient.model";
import { ShoppingListService } from "src/app/shared/shopping-list.service";
import { NgForm } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";

@Component({
    selector: 'app-shopping-listEdit',
    templateUrl: './shoppingListEdit.component.html'
})

export class ShoppingListEditComponent implements OnInit, OnDestroy {

    @ViewChild('f') form: NgForm;
    editMode = false;
    subs: Subscription;
    editedItemIndex: number;
    editedIngredient: Ingredient;

    constructor(private shoppingListService: ShoppingListService, private route: ActivatedRoute) { }

    ngOnInit() {
        this.subs = this.shoppingListService.startedEdit.subscribe(
            (index: number) => {
                this.editMode = true;
                this.editedItemIndex = index;
                this.editedIngredient = this.shoppingListService.getIngredient(index);
                this.form.setValue({
                    name: this.editedIngredient.name,
                    quantity: this.editedIngredient.quantity,
                });
            }
        );

    }
    onSubmit() {
        var newIngredient: Ingredient = new Ingredient(this.form.value.name, this.form.value.quantity);
        if (!this.editMode) {
            this.shoppingListService.addIngredient(newIngredient);
        } else {
            this.shoppingListService.updateIngredient(newIngredient, this.editedItemIndex);
        }
        this.form.reset();
        this.editMode = false;
        console.log(this.form);
    }

    onClear() {
        this.form.reset();
        this.editMode = false;
    }

    onDelete() {
        this.shoppingListService.deleteIngredient(this.editedItemIndex);
        this.editMode = false;
        this.form.reset();
    }

    ngOnDestroy() {
        this.subs.unsubscribe();
    }
}