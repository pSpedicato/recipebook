import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RecipesComponent } from './recipes.component';
import { SelectRecipeComponent } from './select-recipe/select-recipe.component';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';
import { AuthGuardService } from '../auth/auth-guard.service';
import { RecipeDetailComponent } from './recipeDetail/recipeDetail.component';

const recipesRoutes: Routes = [
  {
    path: '', component: RecipesComponent, children: [
      { path: '', component: SelectRecipeComponent },
      { path: 'new', component: RecipeEditComponent, canActivate: [AuthGuardService] },
      { path: ':index', component: RecipeDetailComponent },
      { path: ':index/edit', component: RecipeEditComponent, canActivate: [AuthGuardService] },
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(recipesRoutes),
  ],
  declarations: [],
  exports: [RouterModule],
  providers: [
    AuthGuardService,
  ]

})
export class RecipesRoutingModule { }
