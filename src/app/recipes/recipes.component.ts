import { Component, OnInit } from '@angular/core';
import { Recipe } from './recipe.model';
import { RecipeService } from '../shared/recipe.service';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'app-recipes',
    templateUrl: './recipes.component.html',

})

export class RecipesComponent implements OnInit {
    recipeSelected: Recipe;
    constructor(private recipeService: RecipeService) { }

    ngOnInit() {
        this.recipeService.recipeSelected.subscribe(
            (recipe: Recipe) => { this.recipeSelected = recipe; }
        );
    }

}