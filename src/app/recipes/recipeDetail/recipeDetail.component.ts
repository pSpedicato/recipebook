import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from 'src/app/shared/recipe.service';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { trigger, state, style, transition, animate, useAnimation } from '@angular/animations';
import { getRandomString } from 'selenium-webdriver/safari';

@Component({
    selector: 'app-recipeDetail',
    templateUrl: './recipeDetail.component.html',

})

export class RecipeDetailComponent implements OnInit {
    recipeSelected: Recipe;
    index: number;
    ingredients: Ingredient[];
    constructor(private recipeService: RecipeService, private route: ActivatedRoute,
        private router: Router, private authService: AuthService) { }
    onAddToList() {
        this.recipeService.addToShoppingList(this.recipeSelected.ingredients);
    }

    ngOnInit() {
        this.index = +this.route.snapshot.params['index'];
        this.recipeSelected = this.recipeService.getRecipe(this.index);
        this.route.params.subscribe(
            (params: Params) => {
                this.recipeSelected = this.recipeService.getRecipe(+params['index']);
            }
        );
    }
    isAuthenticated() {
        return this.authService.isAuthenticated();
    }

    onDelete() {
        this.recipeService.deleteRecipe(this.index);
        this.router.navigate(['/recipes']);

    }
}
