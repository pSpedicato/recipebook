import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RecipeService } from 'src/app/shared/recipe.service';
import { Recipe } from '../recipe.model';
import { NgForm, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Ingredient } from 'src/app/shared/ingredient.model';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id: number;
  editMode: boolean;
  @ViewChild('f') form: NgForm;
  r: Recipe = new Recipe('', '', '', []);
  recipeForm: FormGroup;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['index'];
        this.editMode = params['index'] != null;
      }
    );
    this.formInit();
  }
  formInit() {
    let recipeName = '';
    let recipeDescription = '';
    let recipeImagePath = '';
    let recipeIngredients = new FormArray([]);
    if (this.editMode) {
      const recipeSelected = this.recipeService.getRecipe(this.id);
      recipeName = recipeSelected.name;
      recipeDescription = recipeSelected.description;
      recipeImagePath = recipeSelected.imagePath;
      if (recipeSelected['ingredients']) {
        for (let ingredient of recipeSelected.ingredients) {
          recipeIngredients.push(
            new FormGroup({
              'name': new FormControl(ingredient.name, [Validators.required]),
              'quantity': new FormControl(ingredient.quantity, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]),
            })
          );
        }
      }
    }
    this.recipeForm = new FormGroup({
      'name': new FormControl(recipeName, [Validators.required]),
      'description': new FormControl(recipeDescription, [Validators.required]),
      'imagePath': new FormControl(recipeImagePath),
      'ingredients': recipeIngredients,
    });
  }

  getControl() {
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }

  onDeleteRecipe() {
    if (this.editMode) {
      this.recipeService.deleteRecipe(this.id);
      this.router.navigate(['/recipes']);
    }
  }

  onCancel() {
    this.router.navigate(['/recipes']);
  }

  onAddIngredient() {
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.recipeForm.get('ingredients')).push(control);
  }

  onDeleteIngredient(index: number) {
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }

  onAdd() {
    (<FormArray>this.recipeForm.get('ingredients')).push(new FormGroup(
      {
        'name': new FormControl(null, Validators.required),
        'quantity': new FormControl(null, [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/)]),
      }
    ));
  }

  onSubmit() {
    console.log(this.recipeForm);

    this.r.name = this.recipeForm.value.name;
    this.r.description = this.recipeForm.value.description;
    this.r.imagePath = this.recipeForm.value.imagePath;
    this.r.ingredients = this.recipeForm.value.ingredients;
    if (this.editMode) {
      this.recipeService.update(this.id, this.r);
    } else {
      this.recipeService.addRecipe(this.r);

    }
  }

}
