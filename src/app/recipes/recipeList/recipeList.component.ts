import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from 'src/app/shared/recipe.service';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
    selector: 'app-recipeList',
    templateUrl: './recipeList.component.html'
})

export class RecipeListComponent implements OnInit {
    recipes: Recipe[] = [];
    constructor(private recipeService: RecipeService, private auth: AuthService) { }
    ngOnInit() {
        this.recipes = this.recipeService.recipes;
        this.recipeService.recipeChanged.subscribe(
            (recipes: Recipe[]) => {
                this.recipes = recipes;
            }
        );
    }
    isAuthenticated() {
        return this.auth.isAuthenticated();
    }
}
