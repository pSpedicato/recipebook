import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../../recipe.model';
import { RecipeService } from 'src/app/shared/recipe.service';

@Component({
    selector: 'app-recipeItem',
    templateUrl: './recipeItem.component.html'
})

export class RecipeItemComponent {
    @Input() recipe: Recipe;
    @Input() index: number;
    constructor(private recipeService: RecipeService) { }
    // recipeSelected(){
    //     this.recipeService.onRecipeSelected(this.recipe);
    // }
}