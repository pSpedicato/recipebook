import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Response } from '@angular/http';
import { DataStorageService } from '../../shared/data-storage.service';
import { AuthService } from '../../auth/auth.service';
import { HttpEvent } from '@angular/common/http';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
})

export class HeaderComponent {
    constructor(private router: Router, private dataService: DataStorageService, private authService: AuthService) { }

    onSaveData() {
        this.dataService.saveSList().subscribe(
            (response: Response) => console.log(response)
        );
        this.dataService.saveRecipe().subscribe(
            (response) => console.log(response)
        );
    }
    onFetchData() {
        this.dataService.getRecipes();
        this.dataService.fetchSList();
    }

    isAuthenticated() {
        return this.authService.isAuthenticated();
    }
    onLogout() {
        this.authService.logout();
    }
}
