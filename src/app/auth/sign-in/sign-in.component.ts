import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../auth.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  error: string;
  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  onSignin() {
    const email = this.form.value.email;
    const password = this.form.value.password;
    this.authService.singinUser(email, password);
    this.authService.event.subscribe(
      (params) => {
        this.error = params['message'];
      }
    );
  }
}
