import { Injectable, EventEmitter } from '@angular/core';
import * as firebase from 'firebase';
import { Router, Params } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  tk: string;
  error: Params;
  constructor(private router: Router) { }
  event = new EventEmitter<Params>();
  signupUser(email: string, password: string) {
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .catch(
        error => {
          this.event.emit(error);
        }
      );
  }

  singinUser(email: string, password: string) {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .catch(
        error => {
          this.event.emit(error);
          this.error = error;
        }
      ).then(
        response => {
          firebase.auth().currentUser.getIdToken().then(
            (token: string) => {
              this.tk = token;
            }
          );
          if (this.tk != null) {
            this.router.navigate(['/']);
          }
        }
      );
  }

  getToken() {
    firebase.auth().currentUser.getIdToken().then(
      (token: string) => {
        this.tk = token;
      }
    );
    return this.tk;
  }

  isAuthenticated() {
    return this.tk != null;
  }

  logout() {
    firebase.auth().signOut();
    this.tk = null;
  }
}
