import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router, Params } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  error: string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onSignup() {
    const email = this.form.value.email;
    const password = this.form.value.password;
    this.authService.signupUser(email, password);
    this.authService.event.subscribe(
      (params) => {
        if (params === null) {
          console.log(params);
          this.router.navigate(['/signin']);
        } else {
          this.error = params['message'];
        }
      }
    );


  }

}
